﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationScript : MonoBehaviour {

    public float rotSpeed = 10f;
    public float trlSpeed = 1;
    float originalY;

	void Start () {
        this.originalY = this.transform.position.y;
	}
	
	void Update () {

        transform.Rotate(Vector3.up, (rotSpeed * 10) * Time.deltaTime);

        transform.position = new Vector3(transform.position.x,
            originalY + ((float)Mathf.Sin(Time.time) * trlSpeed) , transform.position.z);
    }
}

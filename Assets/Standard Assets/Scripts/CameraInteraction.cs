﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInteraction : MonoBehaviour {


    public GameObject culpritImg;
    public GameObject culprit2Img;
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (culpritImg.activeSelf)
            {
                culpritImg.SetActive(false);
                culprit2Img.SetActive(true);
            }else if (culprit2Img.activeSelf)
            {
                culprit2Img.SetActive(false);
            }
        }
            if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "Camera")
                {
                    culpritImg.SetActive(true);
                }
            }
        }
    }
}

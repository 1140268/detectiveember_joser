﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class TimerScript : MonoBehaviour {

    public float startTime;
    public float endTime;
    private float timer;
    public Text gameTime;
    public GameObject player;

    private void Start()
    {
        timer = startTime;
    }

    // Update is called once per frame
    void Update () {
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
            
            int d = (timer/60 > endTime ? (int)(24 + endTime - (timer/60)) : (int)(endTime - (timer / 60)));
            gameTime.text = (d > 12 ? (d-12).ToString() + "PM" : d.ToString() + "AM");
        }
        else
        {
            player.GetComponent<FirstPersonController>().timesUp();
        }
	}

    public void resetTimer()
    {
        timer = startTime;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class ButtonController : MonoBehaviour
{

    public GameObject victoryCanvas;
    public GameObject defeatCanvas;
    public GameObject firemanCanvas;
    public GameObject mainCanvas;
    public GameObject childplane;
    public GameObject childtext;
    public Text text;
    public Text extraDialogue;
    public GameObject dialogueBox;
    public GameObject culp1Btn;
    public GameObject culp2Btn;
    public GameObject culp3Btn;
    public GameObject culp4Btn;
    public GameObject cause1Btn;
    public GameObject cause2Btn;
    public GameObject cause3Btn;
    public GameObject cause4Btn;
    public GameObject natCause1Btn;
    public GameObject natCause2Btn;
    public GameObject natCause3Btn;
    public GameObject naturalBtn;
    public GameObject intentionalBtn;
    public GameObject accidentalBtn;
    public GameObject player;
    public bool showFiremanCanvas;

    private bool choseRightPerson = false;
    private bool choseRightTool = false;
    private bool choseRightType = false;
    private bool choseRightNatCause = false;
    private GameStrings gameStrings = new GameStrings();

    private void Start()
    {
        if (showFiremanCanvas)
        {
            firemanCanvas.SetActive(true);
        }
    }

    public void restartCanvas()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "Scene1":
                text.text = "Who's the culprit?";
                if (culp1Btn != null)
                    culp1Btn.SetActive(true);
                if (culp2Btn != null)
                    culp2Btn.SetActive(true);
                if (culp3Btn != null)
                    culp3Btn.SetActive(true);
                if (culp4Btn != null)
                    culp4Btn.SetActive(true);
                if (natCause1Btn != null)
                    natCause1Btn.SetActive(false);
                if (natCause2Btn != null)
                    natCause2Btn.SetActive(false);
                if (natCause3Btn != null)
                    natCause3Btn.SetActive(false);
                if (cause1Btn != null)
                    cause1Btn.SetActive(false);
                if (cause2Btn != null)
                    cause2Btn.SetActive(false);
                if (cause3Btn != null)
                    cause3Btn.SetActive(false);
                if (cause4Btn != null)
                    cause4Btn.SetActive(false);
                if(intentionalBtn != null)
                intentionalBtn.SetActive(false);
                if (accidentalBtn != null)
                    accidentalBtn.SetActive(false);
                if (naturalBtn != null)
                    naturalBtn.SetActive(false);
                break;
            case "Scene2":
            case "Scene3":
                text.text = "What type of fire was this?";
                if (culp1Btn != null)
                    culp1Btn.SetActive(false);
                if (culp2Btn != null)
                    culp2Btn.SetActive(false);
                if (culp3Btn != null)
                    culp3Btn.SetActive(false);
                if (culp4Btn != null)
                    culp4Btn.SetActive(false);
                if (natCause1Btn != null)
                    natCause1Btn.SetActive(false);
                if (natCause2Btn != null)
                    natCause2Btn.SetActive(false);
                if (natCause3Btn != null)
                    natCause3Btn.SetActive(false);
                if (cause1Btn != null)
                    cause1Btn.SetActive(false);
                if (cause2Btn != null)
                    cause2Btn.SetActive(false);
                if (cause3Btn != null)
                    cause3Btn.SetActive(false);
                if (cause4Btn != null)
                    cause4Btn.SetActive(false);
                intentionalBtn.SetActive(true);
                accidentalBtn.SetActive(true);
                naturalBtn.SetActive(true);
                break;
        }
    }

    public void Button_Intentional_Click()
    {
        text.text = "Who's the culprit?";
        player.GetComponent<FirstPersonController>().ActivateCulprits();
        intentionalBtn.SetActive(false);
        accidentalBtn.SetActive(false);
        naturalBtn.SetActive(false);
        switch (SceneManager.GetActiveScene().name)
        {
            case "Scene2":
                break;
            case "Scene3":
                break;
        }
    }

    public void Button_Accidental_Click()
    {
        text.text = "Who's the culprit?";
        player.GetComponent<FirstPersonController>().ActivateCulprits();
        intentionalBtn.SetActive(false);
        accidentalBtn.SetActive(false);
        naturalBtn.SetActive(false);
        switch (SceneManager.GetActiveScene().name)
        {
            case "Scene2":
                choseRightType = true;
                break;
            case "Scene3":
                break;
        }
    }

    public void Button_Natural_Click()
    {
        text.text = "What started the fire?";
        natCause1Btn.SetActive(true);
        natCause2Btn.SetActive(true);
        natCause3Btn.SetActive(true);
        intentionalBtn.SetActive(false);
        accidentalBtn.SetActive(false);
        naturalBtn.SetActive(false);
        switch (SceneManager.GetActiveScene().name)
        {
            case "Scene2":
                break;
            case "Scene3":
                choseRightType = true;
                break;
        }
    }

    public void Button_RightPerson_Click()
    {
        text.text = "What started the fire?";
        if (culp1Btn != null)
            culp1Btn.SetActive(false);
        if (culp2Btn != null)
            culp2Btn.SetActive(false);
        if (culp3Btn != null)
            culp3Btn.SetActive(false);
        if (culp4Btn != null)
            culp4Btn.SetActive(false);
        cause1Btn.SetActive(true);
        cause2Btn.SetActive(true);
        cause3Btn.SetActive(true);
        cause4Btn.SetActive(true);
        choseRightPerson = true;
    }

    public void Button_WrongPerson_Click()
    {
        text.text = "What started the fire?";
        if (culp1Btn != null)
            culp1Btn.SetActive(false);
        if (culp2Btn != null)
            culp2Btn.SetActive(false);
        if (culp3Btn != null)
            culp3Btn.SetActive(false);
        if (culp4Btn != null)
            culp4Btn.SetActive(false);
        cause1Btn.SetActive(true);
        cause2Btn.SetActive(true);
        cause3Btn.SetActive(true);
        cause4Btn.SetActive(true);
        Debug.Log("Clicked button Lose");
    }

    public void Button_RightTool_Click()
    {
        mainCanvas.SetActive(false);
        childplane.SetActive(false);
        childtext.SetActive(false);
        cause1Btn.SetActive(false);
        cause2Btn.SetActive(false);
        cause3Btn.SetActive(false);
        cause4Btn.SetActive(false);
        choseRightTool = true;
        if (choseRightPerson && choseRightTool)
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "Scene1":
                    extraDialogue.text = gameStrings.lvl1WinDialogue;
                    dialogueBox.SetActive(true);
                    break;
                case "Scene2":
                    extraDialogue.text = gameStrings.lvl2WinDialogue;
                    dialogueBox.SetActive(true);
                    break;
            }
            victoryCanvas.SetActive(true);
        }
        else
        {
            defeatCanvas.SetActive(true);
        }
    }

    public void Button_RightNatCause_Click()
    {
        mainCanvas.SetActive(false);
        childplane.SetActive(false);
        childtext.SetActive(false);
        natCause1Btn.SetActive(false);
        natCause2Btn.SetActive(false);
        natCause3Btn.SetActive(false);
        choseRightNatCause = true;
        if (choseRightNatCause && choseRightType)
        {
            switch (SceneManager.GetActiveScene().name)
            {
                case "Scene3":
                    extraDialogue.text = gameStrings.lvl3WinDialogue;
                    dialogueBox.SetActive(true);
                    break;
            }
            victoryCanvas.SetActive(true);
        }
        else
        {
            defeatCanvas.SetActive(true);
        }
    }

    public void Button_WrongTool_Click()
    {
        mainCanvas.SetActive(false);
        childplane.SetActive(false);
        childtext.SetActive(false);
        cause1Btn.SetActive(false);
        cause2Btn.SetActive(false);
        cause3Btn.SetActive(false);
        cause4Btn.SetActive(false);
        defeatCanvas.SetActive(true);
    }

    public void Button_WrongNatCause_Click()
    {
        mainCanvas.SetActive(false);
        childplane.SetActive(false);
        childtext.SetActive(false);
        natCause1Btn.SetActive(false);
        natCause2Btn.SetActive(false);
        natCause3Btn.SetActive(false);
        defeatCanvas.SetActive(true);
    }

    public void Button_Continue_Click()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "Scene1":
                SceneManager.LoadScene("Scene2");
                break;
            case "Scene2":
                SceneManager.LoadScene("Scene3");
                break;
            case "Scene3":
                SceneManager.LoadScene("MainMenu");
                break;
        }
        Debug.Log("Clicked button Continue");
    }

    public void Button_Restart_Click()
    {
        player.GetComponent<FirstPersonController>().restart();
        Debug.Log("Clicked button Restart");
    }
}

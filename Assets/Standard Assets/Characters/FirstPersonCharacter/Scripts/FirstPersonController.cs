using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof(CharacterController))]
    [RequireComponent(typeof(AudioSource))]
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] private bool m_IsWalking;
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] private float m_RunSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private float m_JumpSpeed;
        [SerializeField] private float m_StickToGroundForce;
        [SerializeField] private float m_GravityMultiplier;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] private bool m_UseHeadBob;
        [SerializeField] private CurveControlledBob m_HeadBob = new CurveControlledBob();
        [SerializeField] private LerpControlledBob m_JumpBob = new LerpControlledBob();
        [SerializeField] private float m_StepInterval;
        [SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.
        [SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.
        [SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.

        private Camera m_Camera;
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;
        private float m_StepCycle;
        private float m_NextStep;
        private bool m_Jumping;
        private AudioSource m_AudioSource;
        public GameObject textbox;
        public GameObject samsonBtn;
        public GameObject lauraBtn;
        public GameObject billyBtn;
        public GameObject willieBtn;
        public GameObject mathiasBtn;
        public GameObject sarahBtn;
        public GameObject slenderManBtn;
        public GameObject elderManBtn;
        public GameObject mariaBtn;
        public GameObject larryBtn;
        public GameObject daughterBtn;
        public GameObject defeatCanvas;
        public GameObject mainCanvas;
        public GameObject pauseMenu;
        public GameObject optionsMenu;
        public GameObject donnaBtn;
        //public GameObject CanvasMapAndChat;
        public GameObject CanvasTimer;
        public GameObject defeatText;
        public GameObject defeatTimesUpText;
        public Text text;
        GameStrings gameStrings;
        private const int minDetectionDist = 3;

        private float oPosX = 151.29f;
        private float oPosY = 35.64f;
        private float oPosZ = 69.54f;

        private bool spokeToSamson = false;
        private bool spokeToBilly = false;
        private bool spokeToLaura = false;
        private bool spokeToWillie = false;
        private bool spokeToFireman = false;
        private bool spokeToSarah = false;
        private bool spokeToMathias = false;
        private bool spokeToLarry = false;
        private bool spokeToMaria = false;
        private bool spokeToDonna = false;
        private bool lookedAtCamera = false;
        private bool speakingToFireman = false;
        private bool speakingToSamson = false;
        private bool speakingToLaura = false;
        private bool speakingToWillie = false;
        private bool speakingToBilly = false;
        private bool speakingToSarah = false;
        private bool speakingToMathias = false;
        private bool speakingToLarry = false;
        private bool speakingToMaria = false;
        private bool speakingToDonna = false;
        private bool lookingAtCamera = false;

        private string[] dialog;
        private int dialogIt;

        //My objects
        public GameObject firemanCanvas;
        private bool lvl1Loaded;
        private bool lvl2Loaded;
        private bool lvl3Loaded;
        private bool gameIsPaused = false;


        // Use this for initialization
        private void Start()
        {
            oPosX = transform.position.x;
            oPosY = transform.position.y;
            oPosZ = transform.position.z;
            Time.timeScale = 1F;
            gameStrings = new GameStrings();
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle / 2f;
            m_Jumping = false;
            m_AudioSource = GetComponent<AudioSource>();
            m_MouseLook.Init(transform, m_Camera.transform);
            text.text = gameStrings.lvl1StartLine;
            firemanCanvas.SetActive(false);
            if (m_WalkSpeed == 0 && m_RunSpeed == 0)
            {
                m_WalkSpeed = 5;
                m_RunSpeed = 10;
            }
            switch (SceneManager.GetActiveScene().name)
            {
                case "Scene1":
                    lvl1Loaded = true;
                    lvl2Loaded = false;
                    lvl3Loaded = false;
                    text.text = gameStrings.lvl1StartLine;
                    break;
                case "Scene2":
                    lvl1Loaded = false;
                    lvl2Loaded = true;
                    lvl3Loaded = false;
                    text.text = gameStrings.lvl2StartLine;
                    break;
                case "Scene3":
                    lvl1Loaded = false;
                    lvl2Loaded = false;
                    lvl3Loaded = true;
                    text.text = gameStrings.lvl3StartLine;
                    break;
            }

        }

        public void ActivateCulprits()
        {
            if (spokeToSarah)
            {
                sarahBtn.SetActive(true);
            }
            if (spokeToMathias)
            {
                mathiasBtn.SetActive(true);
            }
            if (lookedAtCamera)
            {
                slenderManBtn.SetActive(true);
                elderManBtn.SetActive(true);
            }
            if (spokeToMaria)
            {
                mariaBtn.SetActive(true);
            }
            if (spokeToLarry)
            {
                larryBtn.SetActive(true);
                daughterBtn.SetActive(true);
            }
            if (spokeToDonna)
            {
                donnaBtn.SetActive(true);
            }
        }

        public void Resume()
        {
            Debug.Log("rESUME CLICKED");

            m_MouseLook.lockCursor = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            m_WalkSpeed = 5;
            m_RunSpeed = 10;
                        
            //Canvas
            mainCanvas.SetActive(true);
            CanvasTimer.SetActive(true);
            pauseMenu.SetActive(false);
            optionsMenu.SetActive(false);

            Time.timeScale = 1F;
            gameIsPaused = false;
        }

        void Pause()
        {
            m_MouseLook.lockCursor = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            m_WalkSpeed = 0;
            m_RunSpeed = 0;

            //Canvas
            mainCanvas.SetActive(false);
            CanvasTimer.SetActive(false);
            pauseMenu.SetActive(true);

            Time.timeScale = 0F;
            gameIsPaused = true;

        }

        public void restart()
        {
            GetComponent<TimerScript>().resetTimer();
            mainCanvas.SetActive(true);
            defeatCanvas.SetActive(false);
            defeatText.SetActive(true);
            defeatTimesUpText.SetActive(false);
            spokeToSamson = false;
            spokeToBilly = false;
            spokeToLaura = false;
            spokeToWillie = false;
            spokeToFireman = false;
            spokeToMathias = false;
            spokeToSarah = false;
            transform.position = new Vector3(oPosX, oPosY, oPosZ);
            m_MouseLook.lockCursor = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            m_WalkSpeed = 5;
            m_RunSpeed = 10;
            firemanCanvas.SetActive(false);
            text.text = gameStrings.lvl1StartLine;
            textbox.SetActive(true);
        }

        public void timesUp()
        {
            mainCanvas.SetActive(false);
            m_MouseLook.lockCursor = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            m_WalkSpeed = 0;
            m_RunSpeed = 0;
            defeatText.SetActive(false);
            defeatTimesUpText.SetActive(true);
            defeatCanvas.SetActive(true);
        }

        // Update is called once per frame
        private void Update()
        {
            if (!firemanCanvas.activeSelf && !defeatCanvas.activeSelf && !gameIsPaused)
                RotateView();
            // the jump state needs to read here to make sure it is not missed
            //if (!m_Jump)
            //{
            //    m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            //}

            //if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
            //{
            //    StartCoroutine(m_JumpBob.DoBobCycle());
            //    PlayLandingSound();
            //    m_MoveDir.y = 0f;
            //    m_Jumping = false;
            //}
            //if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
            //{
            //    m_MoveDir.y = 0f;
            //}

            //m_PreviouslyGrounded = m_CharacterController.isGrounded;

            if (Input.GetKeyUp(KeyCode.Space))
            {
                if (firemanCanvas.activeSelf)
                {
                    mainCanvas.SetActive(true);
                    m_MouseLook.lockCursor = true;
                    Cursor.visible = false;
                    Cursor.lockState = CursorLockMode.Locked;
                    m_WalkSpeed = 5;
                    m_RunSpeed = 10;
                    firemanCanvas.GetComponent<ButtonController>().restartCanvas();
                    firemanCanvas.SetActive(false);
                }
                if (textbox.activeSelf)
                {
                    if (speakingToFireman)
                    {
                        if (lvl1Loaded)
                            text.text = gameStrings.lvl1FiremanResponse;
                        if (lvl2Loaded)
                            text.text = gameStrings.lvl2FiremanResponse;
                        if (lvl3Loaded)
                            text.text = gameStrings.lvl3FiremanResponse;

                        speakingToFireman = false;
                        spokeToFireman = true;
                    }
                    else if (speakingToBilly || speakingToLaura || speakingToSamson || speakingToWillie || speakingToSarah || speakingToMathias || speakingToLarry || speakingToMaria || lookingAtCamera || speakingToDonna)
                    {
                        dialogIt++;
                        if (dialogIt >= dialog.Length)
                        {
                            if (speakingToWillie)
                            {
                                spokeToWillie = true;
                                willieBtn.SetActive(true);
                            }
                            if (speakingToLaura)
                            {
                                spokeToLaura = true;
                                lauraBtn.SetActive(true);
                            }
                            if (speakingToSamson)
                            {
                                spokeToSamson = true;
                                samsonBtn.SetActive(true);
                            }
                            if (speakingToBilly)
                            {
                                spokeToBilly = true;
                                billyBtn.SetActive(true);
                            }
                            if (speakingToMathias)
                            {
                                spokeToMathias = true;
                            }
                            if (speakingToSarah)
                            {
                                spokeToSarah = true;
                            }
                            if (speakingToLarry)
                            {
                                spokeToLarry = true;
                            }
                            if (speakingToMaria)
                            {
                                spokeToMaria = true;
                            }
                            if (lookingAtCamera)
                            {
                                lookedAtCamera = true;
                            }
                            if (speakingToDonna)
                            {
                                spokeToDonna = true;
                            }

                            speakingToSamson = false;
                            speakingToLaura = false;
                            speakingToWillie = false;
                            speakingToBilly = false;
                            speakingToMathias = false;
                            speakingToSarah = false;
                            speakingToMaria = false;
                            speakingToLarry = false;
                            lookingAtCamera = false;
                            speakingToDonna = false;
                            textbox.SetActive(false);
                        }
                        else
                        {
                            text.text = dialog[dialogIt];
                        }
                    }
                    else
                    {
                        textbox.SetActive(false);
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.P))
            {

                if (gameIsPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.name == "Player")
                    {
                        Debug.Log("This is a Player");
                    }
                    else if (hit.distance <= minDetectionDist) //Detect the obect, maybe use switch control
                    {
                        //Interact, speak, save object
                        switch (hit.transform.tag)
                        {
                            case "Matches":
                                text.text = gameStrings.matchesClueFound;
                                textbox.SetActive(true);
                                break;
                            case "Jewel":
                                text.text = gameStrings.jewelClueFound;
                                textbox.SetActive(true);
                                break;
                            case "Gasoline":
                                text.text = gameStrings.gasolineClueFound;
                                textbox.SetActive(true);
                                //StartCoroutine(GasolineClueLine());
                                Debug.Log("This is Gasoline");
                                break;
                            case "FullGasoline":
                                text.text = gameStrings.fullGasolineClueFound;
                                textbox.SetActive(true);
                                //StartCoroutine(FullGasolineClueLine());
                                Debug.Log("This is FullGasoline");
                                break;
                            case "Barrel":
                                //StartCoroutine(BarrelClueLine());
                                int number = Random.Range(0, 2);
                                text.text = gameStrings.barrelsClueFound[number];
                                textbox.SetActive(true);
                                Debug.Log("This is a barrel");
                                break;
                            case "Fireman":
                                firemanInteraction();
                                Debug.Log("This is the fireman");
                                break;
                            case "Puddle":
                                int num = Random.Range(0, 2);
                                text.text = gameStrings.puddleClueFound[num];
                                textbox.SetActive(true);
                                break;
                        }

                        switch (hit.transform.name)
                        {
                            case "Bonfire_1":
                                text.text = gameStrings.firePitInteraction;
                                textbox.SetActive(true);
                                //StartCoroutine(WrongFireLine());
                                Debug.Log("This is Firepit");
                                break;
                            case "Samson":
                                dialog = gameStrings.samsonDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToSamson = true;
                                textbox.SetActive(true);
                                break;
                            case "Laura":
                                dialog = gameStrings.lauraDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToLaura = true;
                                textbox.SetActive(true);
                                break;
                            case "Billy":
                                dialog = gameStrings.billyDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToBilly = true;
                                textbox.SetActive(true);
                                break;
                            case "Willie":
                                dialog = gameStrings.willieDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToWillie = true;
                                textbox.SetActive(true);
                                break;
                            case "Mathias":
                                dialog = gameStrings.mathiasDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToMathias = true;
                                textbox.SetActive(true);
                                break;
                            case "Sarah":
                                dialog = gameStrings.sarahDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToSarah = true;
                                textbox.SetActive(true);
                                break;
                            case "Maria":
                                dialog = gameStrings.mariaDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToMaria = true;
                                textbox.SetActive(true);
                                break;
                            case "Larry":
                                dialog = gameStrings.larryDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToLarry = true;
                                textbox.SetActive(true);
                                break;
                            case "Camera":
                                dialog = gameStrings.cameraClueFound;
                                text.text = dialog[0];
                                dialogIt = 0;
                                lookingAtCamera = true;
                                textbox.SetActive(true);
                                break;
                            case "Donna":
                                dialog = gameStrings.donnaDialogue;
                                text.text = dialog[0];
                                dialogIt = 0;
                                speakingToDonna = true;
                                textbox.SetActive(true);
                                break;
                        }

                    }
                }
            }
        }

        private void firemanInteraction()
        {
            if (lvl1Loaded)
            {
                if (spokeToFireman && (spokeToBilly || spokeToLaura || spokeToSamson || spokeToWillie))
                {
                    mainCanvas.SetActive(false);
                    m_MouseLook.lockCursor = false;
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                    m_WalkSpeed = 0;
                    m_RunSpeed = 0;
                    firemanCanvas.SetActive(true);
                }
                else
                {
                    text.text = gameStrings.lvl1FiremanLine;
                    textbox.SetActive(true);
                    speakingToFireman = true;
                }
            }
            else if (lvl2Loaded)
            {
                if (spokeToFireman)
                {
                    m_MouseLook.lockCursor = false;
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                    m_WalkSpeed = 0;
                    m_RunSpeed = 0;
                    firemanCanvas.SetActive(true);
                }
                else
                {
                    text.text = gameStrings.lvl2FiremanLine;
                    textbox.SetActive(true);
                    speakingToFireman = true;
                }
            }
            else
            {
                if (spokeToFireman)
                {
                    m_MouseLook.lockCursor = false;
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                    m_WalkSpeed = 0;
                    m_RunSpeed = 0;
                    firemanCanvas.SetActive(true);
                }
                else
                {
                    text.text = gameStrings.lvl3FiremanLine;
                    textbox.SetActive(true);
                    speakingToFireman = true;
                }
            }

        }

        private IEnumerator BarrelClueLine()
        {
            int number = Random.Range(0, 2);
            text.text = gameStrings.barrelsClueFound[number];
            textbox.SetActive(true);
            yield return new WaitForSeconds(8);
            textbox.SetActive(false);
        }

        private IEnumerator FullGasolineClueLine()
        {
            text.text = gameStrings.fullGasolineClueFound;
            textbox.SetActive(true);
            yield return new WaitForSeconds(8);
            textbox.SetActive(false);
        }

        private IEnumerator WrongFireLine()
        {
            text.text = gameStrings.firePitInteraction;
            textbox.SetActive(true);
            yield return new WaitForSeconds(8);
            textbox.SetActive(false);
        }

        private IEnumerator GasolineClueLine()
        {
            text.text = gameStrings.gasolineClueFound;
            textbox.SetActive(true);
            yield return new WaitForSeconds(8);
            textbox.SetActive(false);
        }

        private void PlayLandingSound()
        {
            m_AudioSource.clip = m_LandSound;
            m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }


        private void FixedUpdate()
        {
            if (!firemanCanvas.activeSelf)
            {
                float speed;
                GetInput(out speed);
                // always move along the camera forward as it is the direction that it being aimed at
                Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

                // get a normal for the surface that is being touched to move along it
                RaycastHit hitInfo;
                Physics.SphereCast(transform.position, m_CharacterController.radius, Vector3.down, out hitInfo,
                                   m_CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
                desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

                m_MoveDir.x = desiredMove.x * speed;
                m_MoveDir.z = desiredMove.z * speed;


                if (m_CharacterController.isGrounded)
                {
                    m_MoveDir.y = -m_StickToGroundForce;

                    //if (m_Jump)
                    //{
                    //    m_MoveDir.y = m_JumpSpeed;
                    //    PlayJumpSound();
                    //    m_Jump = false;
                    //    m_Jumping = true;
                    //}
                }
                else
                {
                    m_MoveDir += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
                }
                m_CollisionFlags = m_CharacterController.Move(m_MoveDir * Time.fixedDeltaTime);

                ProgressStepCycle(speed);
                UpdateCameraPosition(speed);

                m_MouseLook.UpdateCursorLock();
            }
        }


        private void PlayJumpSound()
        {
            m_AudioSource.clip = m_JumpSound;
            m_AudioSource.Play();
        }


        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            PlayFootStepAudio();
        }


        private void PlayFootStepAudio()
        {
            if (!m_CharacterController.isGrounded)
            {
                return;
            }
            // pick & play a random footstep sound from the array,
            // excluding sound at index 0
            int n = Random.Range(1, m_FootstepSounds.Length);
            m_AudioSource.clip = m_FootstepSounds[n];
            m_AudioSource.PlayOneShot(m_AudioSource.clip);
            // move picked sound to index 0 so it's not picked next time
            m_FootstepSounds[n] = m_FootstepSounds[0];
            m_FootstepSounds[0] = m_AudioSource.clip;
        }


        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;
            if (!m_UseHeadBob)
            {
                return;
            }
            if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
            {
                m_Camera.transform.localPosition =
                    m_HeadBob.DoHeadBob(m_CharacterController.velocity.magnitude +
                                      (speed * (m_IsWalking ? 1f : m_RunstepLenghten)));
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }
            m_Camera.transform.localPosition = newCameraPosition;
        }


        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxis("Vertical");

            bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
            // set the desired speed to be walking or running
            speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }


        private void RotateView()
        {
            m_MouseLook.LookRotation(transform, m_Camera.transform);
        }


        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
        }
    }
}

﻿public class GameStrings {
    //Store Strings like Narration and Dialogue

    //Start Lines
    public readonly string lvl1StartLine = "DET. EMBER \n\n\nIt's the 4th fire in 2 months, I must discover who's behind this before more damage is done!";
    public readonly string lvl2StartLine = "DET. EMBER \n\n\nWhat an odd place for a nightly fire, there's no civilization nearby.";
    public readonly string lvl3StartLine = "DET. EMBER \n\n\nWho woulda thunk a fire would take place during a storm?";

    //Fireman
    public readonly string lvl1FiremanLine = "FIREMAN \n\n\nGood morning, detective! The fire was scattered throughout the forest, the campers contacted us right before it reached the tents. You have until 8PM sharp before the campers leave, good luck!";
    public readonly string lvl2FiremanLine = "FIREMAN \n\n\nDetective Ember, we arrived as soon as we could and found no cause! Do your magic, the fire was up ahead, by the street. We leave at 4AM.";
    public readonly string lvl3FiremanLine = "FIREMAN \n\n\nWe need you again, Ember, a large area was turned to cinders and we found no cause. We'll close the area in 6 hours, be quick.";
    public readonly string lvl1FiremanResponse = "DET. EMBER \n\n\nYou can count on me, chief!";
    public readonly string lvl2FiremanResponse = "DET. EMBER \n\n\nA lack of leads provides for an exciting mystery. I'll do my best!";
    public readonly string lvl3FiremanResponse = "DET. EMBER \n\n\nLarge fire, good job putting it out, I'll take it from here!";

    //Victory Lines
    public readonly string lvl1WinDialogue = "DET. EMBER \n\n\nIt all makes sense. Laura desired the boss's jewelry so she took Samson's matches while he slept, started a fire with a fuel canister to draw everyone outside. With the boss distracted, she could steal from him, dump the canister and hope to return home today. Another case cracked!";
    public readonly string lvl2WinDialogue = "DET. EMBER \n\n\nAn unfortunate turn of events, a smoker threw out his cigar out the car window and accidentally started the wildfire. Luckily the child was around to cath those images!";
    public readonly string lvl3WinDialogue = "DET. EMBER \n\n\nNo item was used but ignoring the land made it a whole lot easier for the hot lightning to start that fire. I'm not a spiritual man...but Karma sure finds a way.";


    //Level 1 Dialogue
    //Samson
    public readonly string[] samsonDialogue = new string[] { "DET. EMBER \n\n\nGood afternoon, sir, I'm Detective Ember, I'm here to discover the cause of the fire. I need to know your name and where were you during the fire.",
        "SAMSON \n\n\nSamson's the name and I was asleep, man. Woke up to the sounds of screaming, my vacation is ruined!",
        "DET. EMBER \n\n\nThe randomness doesn't indicate natural causes, do you think anyone was behind this?",
        "SAMSON \n\n\nThe camp owner is a mean dude, I wouldn't be surprised if he was behind it all!",
        "DET. EMBER \n\n\nOne more thing, how long have you been here for?",
        "SAMSON \n\n\nIt's my second day! Sec-ond day!! I'm not coming back! I can't even get a nice bed for myself.."};
    public readonly string samsonStarter = "DET. EMBER \n\n\nWhere were you during the fire?";
    public readonly string samsonResponse = "SAMSON \n\n\nAsleep, man. Woke up to the sounds of screaming, my vacation is ruined!";
    public readonly string responseToSamson = "DET. EMBER \n\n\nThe randomness doesn't indicate natural causes, do you think anyone was behind this?";
    public readonly string samsonResponse2 = "SAMSON \n\n\nThe camp owner is a mean dude, I wouldn't be surprised if he was behind it all.";
    public readonly string responseToSamson2 = "DET. EMBER \n\n\nOne more thing, how long have you been here for?";
    public readonly string samsonResponse3 = "SAMSON \n\n\nIt's my second day! Sec-ond day!! I'm not coming back!";

    //Billy
    public readonly string[] billyDialogue = new string[] { "DET. EMBER\n\n\nExcuse me, boy, I'm supposed to uncover the mystery here, what's your name? What happened last night?",
        "BILLY\n\n\nI'm Billy, mister! And I was roasting marshmallows when the alarm sounded!",
        "DET. EMBER\n\n\nIs that so? Did you see anything suspicious?",
        "BILLY\n\n\nEveryone left their places except for one, I couldn't tell who it was though.",
        "DET. EMBER\n\n\nFinally, what do you think of the other campers?",
        "BILLY\n\n\nSamson is weird and vengeful, Willie doesn't come out of his home much and Laura annoys me, keeps boasting about new things she owns."
    };
    //Laura
    public readonly string[] lauraDialogue = new string[] {"DET. EMBER\n\n\nG'day, ma'am, I'm gonna need your name and phone num-I mean, where you were during the fire.",
    "LAURA\n\n\nI'm Laura and I was just admiring my smoking looks by the lake, what do you think of my new dress?",
    "DET. EMBER\n\n\nYou get a lot of new clothes?",
    "LAURA\n\n\nIt's expensive to stay fashionable, I get rid of all old clothes as soon as I get new ones.",
    "DET. EMBER\n\n\nThe others, what are they like?",
    "LAURA\n\n\nI've been here too long and I've never met an owner this careless, the young boy keeps playing with fire and the new one sleeps all the time."};
    //Willie
    public readonly string[] willieDialogue = new string[] {"WILLIE\n\n\nWhat do you want from Willie now?!",
    "DET. EMBER\n\n\nAnswers. What happened?",
    "WILLIE\n\n\nAnother darn fire, problems keep coming..",
    "DET. EMBER\n\n\nWhat kind of problems?",
    "WILLIE\n\n\nCustomers keep leaving, scared of the fires and my possessions keep going missing. My wife will miss that new necklace..",
    "Det.Ember:\n\n\nI'll get to the bottom of this! Noticed anything out of the ordinary?",
    "WILLIE\n\n\nAll those lousy brats hate me, they could've all done this! It's why I lock myself in most of the time!"};

    //Level 2 Dialogue
    //Sarah
    public readonly string[] sarahDialogue = new string[] {"DET. EMBER\n\n\nQuite the day, yesterday must've been, madam..?",
    "SARAH\n\n\nSarah and no kidding, it's getting worse every year, even with all the precautions we take.",
    "DET. EMBER\n\n\nIs that so? You smoke? Host any barbecues? Clean the dish-I mean, the roads?",
    "LAURA\n\n\nNo, no and yes, I'd never put my only home at risk!",
    "DET. EMBER\n\n\nWell, do you have anything else to report?",
    "LAURA\n\n\nI thought I heard someone run to the woods, probably the same kid trying to prank me, maybe this time it backfired!"};

    //Mathias
    public readonly string[] mathiasDialogue = new string[] {"DET. EMBER\n\n\nHey, what are you doing over here?",
    "MATHIAS\n\n\nSir, please, I did nothing wrong this time!",
    "DET. EMBER\n\n\nCalm down! Tell me what happened.",
    "MATHIAS\n\n\nI was just working on a school project, mister! I was taking pictures all over the place until I saw the fire, I just ran to safety!",
    "DET. EMBER\n\n\nAt least you're safe, get back home and I promise I'll return the camera!",
    "MATHIAS\n\n\nThank you so much, maybe I can use footage of the fire! That'll be some spicy content!"};

    //Level 3 Dialogue
    //Larry
    public readonly string[] larryDialogue = new string[] {"DET. EMBER\n\n\nYou happen to live here, sir?",
        "LARRY\n\n\nLive? I own the place, I'm taking break, I can't think about all I lost.",
    "DET. EMBER\n\n\nThink anyone could've done this?",
    "LARRY\n\n\nCertainly not me, I can't be bothered to do anything around here, let alone ruin my fortune.",
    "DET. EMBER\n\n\nAnyone else?",
    "LARRY\n\n\nMy daughter is too young to walk but my gran is always nagging and yelling at me, always telling me to work. Work...for what? I'm already rich!"};

    //Maria
    public readonly string[] mariaDialogue = new string[] {"DET. EMBER\n\n\nThis beach sure lacks some sights..",
        "MARIA\n\n\nWhat's that, sonny? You need to speak up, the long thundering affected my poor hearing!",
    "DET. EMBER\n\n\nNevermind that, I need to know if there are any hazards here.",
    "MARIA\n\n\nNone, I make sure to keep everything tidy in the house!",
    "DET. EMBER\n\n\nYou take care of all the housework?",
    "MARIA\n\n\nHouse, yes, I can't do anything beyond the mansion's walls. I can't believe so much is gone, who could've done this.."};

    //Maria
    public readonly string[] donnaDialogue = new string[] {"DET. EMBER\n\n\nAnd who might you be?",
        "DONNA\n\n\nDonna, the gardener.",
    "DET. EMBER\n\n\nGardener? You must be devastated by this!",
    "DONNA\n\n\nTrue but I can't say I'm surprised.",
    "DET. EMBER\n\n\nReally? Why is that?",
    "MARIA\n\n\nI've been asking my boss time and time again for funds to clean up the whole place but he does nothing but spend his wealth having fun while everything else dries up."};

    //Items
    public readonly string matchesClueFound = "DET. EMBER \n\n\nOne of these matches is missing, could've been used to start the fire..";
    public readonly string[] cameraClueFound = new string[] { "DET. EMBER \n\n\nThis camera caught a traveller on the road, a skinny trucker, those get in trouble often.", "DET. EMBER \n\n\nOh, another one! An elderly man, looks like he has something in his mouth while driving..at least he isn't texting." };
    public readonly string jewelClueFound = "DET. EMBER \n\n\nWomen's jewelry, who would hide these?";
    public readonly string gasolineClueFound = "DET. EMBER \n\n\nEmpty and ditched in deep waters, could be our cause!";
    public readonly string fullGasolineClueFound = "DET. EMBER \n\n\nFull and brand new fuel containers.";
    public readonly string[] barrelsClueFound = new string[] {"DET. EMBER \n\n\nWhat an unsanitary place..", "DET. EMBER \n\n\nThese fuel barrels are empty and rusty." };
    public readonly string[] puddleClueFound = new string[] { "DET. EMBER \n\n\nSeems like water was used to fight the fire.", "DET. EMBER \n\n\nClear skies and puddles this big?" };
    public readonly string firePitInteraction = "DET. EMBER \n\n\nThis is not the fire source I'm looking for.";
    public readonly string clueFound = "DET. EMBER \n\n\nA clue, I'm getting warmer.";
    public readonly string whereQ = "DET. EMBER \n\n\nWhere were you during the fire?";
    public readonly string aha = "DET. EMBER \n\n\nAha! The cause of the fire!";
    public readonly string[] lvl1Analysis = new string[] { "DET. EMBER \n\n\nAppears to be a random patch, there's devious work behind this.", "DET. EMBER \n\n\nIt stinks of fuel..", "DET. EMBER \n\n\nThey used a Carbon Dioxide based fire extinguisher, water didn't do it?" };
}

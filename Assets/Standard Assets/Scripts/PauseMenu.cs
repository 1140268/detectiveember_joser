﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour {

    public GameObject player;

    public void Resume()
    {
        player.GetComponent<FirstPersonController>().Resume();
    }

    public void LoadMenu()
    {
        //Time.timeScale = 1F;
        Debug.Log("Loading Menu");
        SceneManager.LoadSceneAsync("MainMenu");
    }

    public void QuitGame()
    {
        Debug.Log("Quitting Menu");
        Application.Quit();
    }
}

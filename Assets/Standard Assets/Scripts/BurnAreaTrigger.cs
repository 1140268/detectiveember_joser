﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BurnAreaTrigger : MonoBehaviour
{
    public GameObject textbox;
    public Text text;
    GameStrings gameStrings;

    // Use this for initialization
    void Start()
    {
        gameStrings = new GameStrings();
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        textbox.SetActive(true);
        int number = Random.Range(0, 3);
        text.text = gameStrings.lvl1Analysis[number];
    }

    private void OnTriggerExit(Collider other)
    {
        textbox.SetActive(false);
    }
}

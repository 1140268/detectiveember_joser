﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    public Slider musicVolume;
    public AudioSource music;
    public Slider soundEffectsVolume;
    public AudioSource soundEffects;

   

	void Update () {
        music.volume = musicVolume.value;
        soundEffects.volume = soundEffectsVolume.value;

    }
}

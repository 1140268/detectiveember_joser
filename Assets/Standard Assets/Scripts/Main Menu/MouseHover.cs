﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MouseHover : MonoBehaviour
{


    public GameObject startBtn;
    public GameObject instructionsBtn;
    public GameObject quitBtn;
    public GameObject intro;
    public GameObject instructionCanvas;
    public GameObject levelSelectCanvas;

    // Use this for initialization
    void Start()
    {
    }

    private void OnMouseEnter()
    {
        MeshRenderer img = GetComponent<MeshRenderer>();
        img.material.color = new Color(219f / 255f, 205f / 255f, 104f / 255f, 189f / 255f);
    }

    private void OnMouseExit()
    {
        MeshRenderer img = GetComponent<MeshRenderer>();
        img.material.color = new Color(204f / 255f, 204f / 255f, 204f / 255f, 189f / 255f);
    }

    public void StartBtn()
    {
        intro.SetActive(false);
        startBtn.SetActive(false);
        instructionsBtn.SetActive(false);
        quitBtn.SetActive(false);
        levelSelectCanvas.SetActive(true);
    }


    public void InstructionBtn()
    {
        intro.SetActive(false);
        startBtn.SetActive(false);
        instructionsBtn.SetActive(false);
        instructionCanvas.SetActive(true);
        quitBtn.SetActive(false);
    }
     
    public void BackButton(){
            intro.SetActive(true);
            startBtn.SetActive(true);
            instructionsBtn.SetActive(true);
            quitBtn.SetActive(true);
            instructionCanvas.SetActive(false);
            levelSelectCanvas.SetActive(false);
    }

    public void SelectLevelBtn(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
